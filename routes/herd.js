var express = require('express');
var router = express.Router();

router.get('/herd', function(req, res) {
    var db = req.db;
    var collection = db.get('unicorns');
    collection.find({},{},function(e,docs){
        res.json(docs);
    });
});

module.exports = router;
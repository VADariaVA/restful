# README #

### This is repository for my first app using RESTful architecture ###

The functionality: this application retrieves data from a client and stores it in the database.
Version 0.0.1

### How do I get set up? ###

I had to install express-generator for this project.
```
#!

npm install -g express-generator
```
Then, starting command prompt in working directory, I typed "express". It auto creates some structure with folders and files that I could use as starting point for developing my application. 
All dependencies described in package.json. Using "npm install" I've installed all modules that I need.
My app uses MongoDB as its database running on 27017 port. 
I have no tests for the application
_________________________________________________________________________________
**If you want to run this application on your machine, follow this instruction:**

   * you need MongoDB to be installed on your machine and running mongo daemon.

   * clone this repository

   * run command prompt in a project directory

   * type npm install (node_modules folder appears)

   * configure db name and address as you need in app.js

   * type npm start 

   * follow the link [http://localhost:3000/](Link URL) and you will     see...nothing.

   * add some data to your db

   * try to follow the link again


### Repo owner ###
Name: Daria Vasilyeva
You contact me via:
E-mail: dashastrange@gmail.com
var express = require('express');
var router = express.Router();

//add
router.post('unicorns/:id', function(req, res) {
    var db = req.db;
    var _id = req.body.id;
    var uniName = req.body.name;
    var uniAge = req.body.age;
    var collection = db.get('unicorns');
    collection.insert({
        "name" : uniName,
        "age" : uniAge,
        "id" : _id
    }, function (err, doc) {
        if (err) {
            res.send(500);
        } else {
            res.json(doc);
        }
    });
});

//find
router.get('unicorns/:id', function(req, res) {
    var db = req.db;
    var _id = req.body.id;
    var collection = db.get('unicorns');
    collection.find({ 
        "id" : _id
    },{},function(e,doc){
        res.json(doc);
    });
});

//delete
router.post('unicorns/:id', function(req, res) {
    var db = req.db;
    var _id = req.body.id;
    var collection = db.get('unicorns');
    collection.remove({ 
        "id" : _id
    },{},function(e,doc){
        res.send(200);
    });
});

module.exports = router;
 
